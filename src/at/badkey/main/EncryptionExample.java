package at.badkey.main;

import at.badkey.decryption.Decrypter;
import at.badkey.encryption.Encrypter;

public class EncryptionExample {
	
	public static void main(String[] args) {
		Encrypter e = new Encrypter();
		String encryptedString = e.encrypt("ZzAa");
		
		System.out.println("Encryption: "+encryptedString);
		
		Decrypter d = new Decrypter();
		String decryptedString = d.decrypt(encryptedString);
		System.out.println("Decryption: "+decryptedString);
		
		
	}

}
