package at.badkey.encryption;

public class Encrypter {

	EncryptionAlgorithm e = new CaesarEncryption();
	
	public void setEncryptionAlgorithm(EncryptionAlgorithm ea) {
		this.e = ea;
	}
	
	public String encrypt(String s) {
		
		return e.encrypt(s);
	}
	
}
