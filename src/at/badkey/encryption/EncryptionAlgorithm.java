package at.badkey.encryption;

public interface EncryptionAlgorithm {
	
	public String encrypt(String textToEncrypt);

}
