package at.badkey.encryption;

import at.badkey.exceptions.NotAlphabeticException;

public class CaesarEncryption implements EncryptionAlgorithm {

	@Override
	public String encrypt(String textToEncrypt) {
		String encryptedString = "";

		for (int i = 0; i < textToEncrypt.length(); i++) {
			char c = textToEncrypt.charAt(i);
			if (Character.isUpperCase(c) && Character.isLetter(c)) {
				
				int ascii = (int) c;

				if ((ascii + 2) > 90) {
					ascii = ascii - 24;
				} else {
					ascii = ascii + 2;
				}

				encryptedString += (char) ascii;

			} else if (Character.isLowerCase(c) && Character.isLetter(c)) {

				int ascii = (int) c;

				if ((ascii + 2) > 122) {
					ascii = ascii - 24;
				} else {
					ascii = ascii + 2;
				}

				encryptedString += (char) ascii;

			} else {
				throw new NotAlphabeticException();
			}
		}

		return encryptedString;
	}

}
