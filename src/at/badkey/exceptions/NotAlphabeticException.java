package at.badkey.exceptions;

public class NotAlphabeticException extends RuntimeException{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotAlphabeticException () {
		System.out.println("Char is not in the alphabet");
	}

}
