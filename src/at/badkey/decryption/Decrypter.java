package at.badkey.decryption;


public class Decrypter {
	DecryptionAlgorithm e = new CaesarDecryption();
	
	public void setDecryptionAlgorithm(DecryptionAlgorithm ea) {
		this.e = ea;
	}
	
	public String decrypt(String s) {
		
		return e.decrypt(s);
	}
}
