package at.badkey.decryption;

import at.badkey.exceptions.NotAlphabeticException;

public class CaesarDecryption implements DecryptionAlgorithm{

	@Override
	public String decrypt(String textToDecrypt) {
		String decryptedString = "";

		for (int i = 0; i < textToDecrypt.length(); i++) {
			char c = textToDecrypt.charAt(i);
			if (Character.isUpperCase(c) && Character.isLetter(c)) {
				
				int ascii = (int) c;

				if ((ascii - 2) < 65) {
					ascii = ascii + 24;
				} else {
					ascii = ascii - 2;
				}

				decryptedString += (char) ascii;

			} else if (Character.isLowerCase(c) && Character.isLetter(c)) {

				int ascii = (int) c;

				if ((ascii - 2) < 97) {
					ascii = ascii + 24;
				} else {
					ascii = ascii - 2;
				}

				decryptedString += (char) ascii;

			} else {
				throw new NotAlphabeticException();
			}
		}

		return decryptedString;
	}
	
}
