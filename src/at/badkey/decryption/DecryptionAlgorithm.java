package at.badkey.decryption;

public interface DecryptionAlgorithm {
	public String decrypt(String textToDecrypt);
}
